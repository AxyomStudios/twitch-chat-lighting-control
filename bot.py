import os
import requests
from playsound import playsound
from webcolors import name_to_rgb
from twitchio.ext import commands

restrictToSubs = False

goveePayload = {
    'device': os.environ['GOVEE_DEVICE'],
    'model': os.environ['GOVEE_MODEL'],
}

async def sendPayload(goveePayload, elgatoPayload=False):
    if goveePayload != False:
        requests.put(
            os.environ['GOVEE_URL'],
            headers={'Govee-API-Key':os.environ['GOVEE_API_KEY']},
            json=goveePayload
        )
    
    if elgatoPayload != False:
        requests.put(
            os.environ['ELGATO_URL'],
            json=elgatoPayload
        )

class Bot(commands.Bot):
    def __init__(self):
        super().__init__(
            token=os.environ['TWITCH_OAUTH_TOKEN'],
            prefix=os.environ['BOT_PREFIX'],
            initial_channels=[os.environ['CHANNEL']]
        )

    async def event_ready(self):
        print(f'Logged in as | {self.nick}')
        print(f'User ID is | {self.user_id}')

    # Turn off lights
    @commands.cooldown(rate=1, per=30, bucket=commands.Bucket.channel)   
    @commands.command()
    async def dark(self, ctx: commands.Context):
        if ctx.author.is_subscriber or restrictToSubs == False:
            goveePayload.update({
                'cmd': {
                    'name': 'turn',
                    'value': 'off'
                }
            })

            elgatoPayload = {
                "lights": [{
                    "on": 0
                }]
            }

            playsound('audio/going-dark.wav')

            await sendPayload(goveePayload, elgatoPayload)
            await ctx.send(f'Bravo six going dark')
        else:
            await ctx.send(f'{ctx.author.name} this command is currently only available to Subscribers')

    # Turn on lights
    @commands.cooldown(rate=1, per=30, bucket=commands.Bucket.channel)
    @commands.command()
    async def light(self, ctx: commands.Context):
        if ctx.author.is_subscriber or restrictToSubs == False:
            goveePayload.update({
                'cmd': {
                    'name': 'turn',
                    'value': 'on'
                }
            })

            elgatoPayload = {
                "lights": [{
                    "on": 1
                }]
            }

            playsound('audio/lights-on.wav')

            await sendPayload(goveePayload, elgatoPayload)
            await ctx.send(f'{ctx.author.name} you really light up my world')
        else:
            await ctx.send(f'{ctx.author.name} this command is currently only available to Subscribers')

    # Change Govee colour
    @commands.cooldown(rate=1, per=30, bucket=commands.Bucket.channel)
    @commands.command()
    async def colour(self, ctx: commands.Context, colourName):
        if ctx.author.is_subscriber or restrictToSubs == False:

            try:
                rgb=name_to_rgb(colourName)
            except:
                await ctx.send(f'{ctx.author.name} is this a real colour? I know these colours: https://www.w3schools.com/cssref/css_colors.php')
            else:
                goveePayload.update({
                    'cmd': {
                        'name': 'color',
                        'value': {
                            "r":rgb.red,
                            "g":rgb.green,
                            "b":rgb.blue
                        }
                    }
                })

                await sendPayload(goveePayload)
                await ctx.send(f'{ctx.author.name} is {colourName} your favourite colour?')
        else:
            await ctx.send(f'{ctx.author.name} this command is currently only available to Subscribers')

    # I fucking hate you all.......
    @commands.cooldown(rate=1, per=30, bucket=commands.Bucket.channel)
    @commands.command()
    async def flashbang(self, ctx: commands.Context):
        if ctx.author.name == '': # Zombie's idea....
            playsound('audio/epic.wav')
        else: 
            if ctx.author.is_subscriber or restrictToSubs == False:

                elgatoPayload1 = {
                    "lights": [{
                        "on": 1,
                        "brightness": 100
                    }]
                }

                elgatoPayload2 = {
                    "lights": [{
                        "on": 1,
                        "brightness": 3
                    }]
                }

                playsound('audio/flashbang-1.wav')
                await sendPayload(False, elgatoPayload1)
                playsound('audio/flashbang-2.wav')
                await sendPayload(False, elgatoPayload2)
                await ctx.send(f'{ctx.author.name.upper()} THREW A FUCKING FLASHBANG!')

            else:
                await ctx.send(f'{ctx.author.name} this command is currently only available to Subscribers')

bot = Bot()
bot.run()

if __name__ == "__main__":
    bot.run()